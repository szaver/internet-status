import React, { Component } from 'react'
import styled, { keyframes } from 'styled-components'
import FaIcon from '@fortawesome/react-fontawesome'
import {
  faArrowDown,
  faCheckCircle as faCheck,
} from '@fortawesome/fontawesome-free-solid'
import 'typeface-lato'
import 'typeface-slabo-27px'

const Container = styled.div`
  display: flex;
  min-height: 100vh;
  font-size: 3rem;
  flex-direction: column;
  align-items: center;
  padding: 3rem;
  box-sizing: border-box;
`

const Content = styled.div`
  font-family: lato;
  flex: none;
  justify-content: center;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  max-width: 1000px;
`

const Title = styled.h1`
  font-family: 'slabo 27px';
  font-size: 5rem;
  margin-bottom: 4rem;
  margin-top: 0;
  flex: none;
`

const bounce = keyframes`
  0% { bottom: 0; }
  50% { bottom: -0.2em; }
  70% { bottom: -0.3em; }
  100% { bottom: 0; }
`

const ErrorMessage = styled.div`
  background: #fff3cd;
  border: 1px solid #ffeeba;
  color: #856404;
  padding: 0.75rem 1.25rem;
  margin-bottom: 3rem;
  border-radius: 0.25rem;
  font-size: 2rem;
`

const TestResult = styled.div`
  ${p => p.status === 'testing' && 'color: lightgray;'};
  ${'' /* ${p => p.status === 'unreachable' && 'color: red;'};
  ${p => p.status === 'ok' && 'color: green;'}; */};
`
const ResultIcon = styled(FaIcon)`
  position: relative;
  ${p => p.status === 'testing' && 'color: lightgray;'};
  ${p => p.status === 'unreachable' && 'color: red;'};
  ${p => p.bounce && `animation: ${bounce} 0.8s infinite linear;`};
  ${'' /* ${p => p.status === 'ok' && 'color: green;'}; */};
  margin: 0.5em;
`

const SuccessCheck = styled(FaIcon)`
  color: #34b434;
  font-size: 0.8em;
`

class App extends Component {
  constructor() {
    super()
    this.state = {
      tests: [],
    }
  }
  runTest(siteInfo) {
    // simple mechanism to cancel older tests
    const testId = Date.now() + '-' + Math.random()
    this.currentTest = testId

    this.setState(
      {
        tests: siteInfo.pingChain.map(dst => {
          return {
            name: dst.name,
            status: 'testing',
            error: dst.error,
            badBrowser: false,
          }
        }),
      },
      () => {
        siteInfo.pingChain.forEach(async (dst, i) => {
          const status = (await checkSite(dst.ping)) ? 'ok' : 'unreachable'
          if (testId === this.currentTest) {
            const newTests = [...this.state.tests]
            newTests[i] = Object.assign({}, this.state.tests[i], { status })
            this.setState({ tests: newTests })
          }
        })
      },
    )
  }
  async componentDidMount() {
    const Modernizr = window.Modernizr
    if (
      !(
        Modernizr.fetch &&
        Modernizr.localstorage &&
        Modernizr.peerconnection &&
        Modernizr.serviceworker
      )
    ) {
      return this.setState({ badBrowser: true })
    }

    const cachedSiteInfo = getCachedConfig()
    if (cachedSiteInfo) {
      console.log('running test from cached info')
      await this.runTest(cachedSiteInfo)
    }

    onIpChange(async ip => {
      console.log('found a new local ip', ip)
      try {
        const siteInfo = await getSiteConfig(ip)
        console.log('running test with new site info')
        this.runTest(siteInfo)
      } catch (e) {
        console.log(e)
      }
    })
  }
  render() {
    let previousFailure = false
    let previousPending = false
    let errorMsg

    for (let test of this.state.tests) {
      if (test.status === 'unreachable') {
        errorMsg = test.error
        break
      }
    }

    const results = this.state.tests.map(test => {
      const hadFailure = previousFailure
      const hadPending = previousPending

      if (test.status === 'unreachable') {
        previousFailure = true
      } else if (test.status === 'testing') {
        previousPending = true
      }

      return [
        <ResultIcon
          key={test.name + 'icon'}
          icon={faArrowDown}
          status={hadFailure || hadPending ? 'testing' : test.status}
          bounce={test.status === 'testing' && !(hadFailure || hadPending)}
        />,
        <TestResult
          key={test.name + 'result'}
          status={previousFailure || previousPending ? 'testing' : test.status}
        >
          {test.name}{' '}
          {(previousFailure || previousPending ? 'testing' : test.status) ===
            'ok' && <SuccessCheck icon={faCheck} />}
        </TestResult>,
      ]
    })

    return (
      <Container>
        <Content>
          <Title>Prueba de Internet</Title>

          {errorMsg && <ErrorMessage>{errorMsg}</ErrorMessage>}
          {this.state.badBrowser ? (
            <ErrorMessage>
              Este navegador no tiene sufficiente capabilidades para
              diagnosticar su conexion. Recomendamos Firefox o Google Chrome en
              un Mac, PC, o Android.
            </ErrorMessage>
          ) : (
            [
              <TestResult status="ok" key="firstResult">
                Este Navegador
              </TestResult>,
              results,
            ]
          )}
        </Content>
      </Container>
    )
  }
}

export default App

function onIpChange(cb) {
  if (typeof localStorage.ipOverride === 'string') {
    return cb(localStorage.ipOverride)
  }

  window.RTCPeerConnection =
    window.RTCPeerConnection ||
    window.mozRTCPeerConnection ||
    window.webkitRTCPeerConnection ||
    false

  if (window.RTCPeerConnection) {
    var pc = new RTCPeerConnection({ iceServers: [] }),
      noop = function() {}
    pc.createDataChannel('')
    pc.createOffer(pc.setLocalDescription.bind(pc), noop)

    pc.onicecandidate = function(event) {
      if (event && event.candidate && event.candidate.candidate) {
        var s = event.candidate.candidate.split('\n')
        cb(s[0].split(' ')[4])
      }
    }
  }
}

const getCachedConfig = () => {
  try {
    return JSON.parse(localStorage.siteConfig)
  } catch (e) {
    return undefined
  }
}

const getSiteConfig = async ip => {
  const ipSegments = ip.split('.')
  const prefix = [ipSegments[0], ipSegments[1], ipSegments[2]].join('.')

  const response = await fetch(`/site-info/${prefix}.json`, {
    method: 'GET',
  })
  const result = await response.json()

  localStorage.siteConfig = JSON.stringify(result, null, 2)

  return result
}

const checkSite = async url => {
  const start = Date.now()
  try {
    await timeout(
      5000,
      fetch(url, {
        method: 'GET',
        mode: 'no-cors',
      }),
    )
    return Date.now() - start
  } catch (e) {
    console.log(e)
    return false
  }
}

function timeout(ms, promise) {
  return new Promise(function(resolve, reject) {
    setTimeout(function() {
      reject(new Error('timeout'))
    }, ms)
    promise.then(resolve, reject)
  })
}
