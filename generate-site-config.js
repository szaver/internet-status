const fs = require('mz/fs')
const got = require('got')
const Table = require('cli-table')
const chalk = require('chalk')
const R = require('ramda')
const path = require('path')
const inquirer = require('inquirer')

const makeTest = (name, ping, error) => {
  if (!ping.startsWith('http')) {
    ping = 'https://' + ping
  }

  return { name, ping, error }
}

const errors = {
  local: `No se puede connectar al antena en su casa.  Podria estar desenchufado.`,
  network: `Su antena no esta recibiendo señal inalambrico.  Podria ser obstruido o girado.  Porfavor revise el antenna en su techo y contactenos si no hay problemas visibles.`,
  futa: `No se puede connectar a Futaleufú.  En general eso significa que hubo un corte de luz en Futaleufú o Noroeste.`,
  internet: `Nuestro red está funcionando bien, pero hay un corte en la red de Telsur.`,
}

const locations = {
  internet: makeTest('Internet', 'https://google.com', errors.internet),
  fernando: makeTest('Futaleufú', '10.0.9.1', errors.futa),
}

const rootLocations = [locations.fernando, locations.internet]

const makeSite = (name, prefix, local, repeater) => {
  return {
    name,
    prefix,
    pingChain: [
      makeTest('Su Casa', local, errors.local),
      makeTest('Offgrid Network', repeater, errors.network),
      ...rootLocations,
    ],
  }
}

const sites = [
  // Morales
  makeSite('Morales', '192.168.23', '10.0.13.1', '10.0.5.8'),
  // Jong Pak
  makeSite('Jong Pak', '192.168.24', '10.0.11.1', '10.0.10.4'),
  // Nestor
  makeSite('Nestor', '192.168.9', '10.0.10.2', '10.0.5.5'),
  // Albert
  // makeSite('Albert', '192.168.1', '10.0.12.51', '10.0.12.50'),
  // Jose Luis
  makeSite('Jose Luis', '192.168.14', '10.0.12.52', '10.0.12.50'),
  // Cornelio
  // makeSite('Cornelio', '', '10.0.8.4', '10.0.4.4'),
  {
    name: 'Castle',
    prefix: '192.168.20',
    pingChain: [
      makeTest('Antenna on roof', '10.0.5.2'),
      makeTest('Albert Mtn', '10.0.4.1'),
      makeTest('Nasario', '10.0.3.1'),
      makeTest('Macarena', '10.0.2.1'),
      makeTest('Dino', '10.0.1.1'),
      locations.internet,
    ],
  },
  {
    name: 'dnet',
    prefix: '10.0.0',
    pingChain: [
      makeTest('Antenna on roof', '10.0.5.2'),
      makeTest('Albert Mtn', '10.0.4.1'),
      makeTest('Nasario', '10.0.3.1'),
      makeTest('Macarena', '10.0.2.1'),
      makeTest('Dino', '10.0.1.1'),
      locations.internet,
    ],
  },
]

main()

async function main() {
  // validate the config
  let cleanRun = true
  for (let site of sites) {
    console.log(chalk.bold.underline.blue(site.name))
    const table = new Table({
      head: ['Location', 'Status'],
    })
    const start = Date.now()

    const ok = () => {
      const time = Date.now() - start
      statusMsg = chalk.green(`OK - ${time}ms`)
    }

    const err = () => {
      cleanRun = false
      statusMsg = chalk.red(`ERR`)
    }

    for (dst of site.pingChain) {
      try {
        await got(dst.ping, { timeout: 5000 })
        ok()
      } catch (e) {
        if (['DEPTH_ZERO_SELF_SIGNED_CERT'].includes(e.code)) {
          ok()
        } else {
          console.log(e)
          err()
        }
      }
      table.push([dst.name, statusMsg])
    }

    console.log(table.toString())
  }

  if (!cleanRun) {
    const { continueAnyway } = await inquirer.prompt([
      {
        type: 'confirm',
        name: 'continueAnyway',
        message: 'Some sites are not responding.  Continue anyway?',
      },
    ])

    if (!continueAnyway) {
      console.log(chalk.italic('Good bye!'))
      return
    }
  }

  console.log('Writing out files...')

  for (site of sites) {
    const sanitizedObject = R.pick(['pingChain'], site)
    const fileContent = JSON.stringify(sanitizedObject, null, 2)

    await fs.writeFile(
      path.join(__dirname, 'public/site-info', site.prefix + '.json'),
      fileContent,
    )
  }

  console.log(chalk.green('All Done!'))
  // force quit in spite of potential lingering fetch events.
  process.exit()
}
